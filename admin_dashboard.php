<?php 
header("Access-Control-Allow-Origin: *");
error_reporting(1);
require 'vendor/autoload.php';
use phpish\shopify;
require 'conf.php';

if (!empty($_SESSION['shop'])) {
 $shop = $_SESSION['shop'];
 $oauth_token = $_SESSION['oauth_token'];
} else if(!empty($_REQUEST['shop'])){
 $shop = $_REQUEST['shop'];
 $oauth_token = $_REQUEST['oauth_token'];
} else {
  die("Session Expire please reopen the app");
}

$shopify = shopify\client($shop, SHOPIFY_APP_API_KEY , $oauth_token);

?>
<! DOCTYPE html>  
<html lang="en">  
<head>  
<title></title>  
<meta charset="utf-8">  
<meta name="viewport" content="width=device-width, initial-scale=1">  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> </script>
<link rel="stylesheet" href="https://unpkg.com/@shopify/polaris@5.0.0/dist/styles.css"/>
</head> 
<body> 

<?php 

// get all products
$all_products = $shopify('GET /admin/api/'.API_VERSION.'/products.json');

echo "Products data : <pre>";
print_r($all_products);
echo "</pre>";


// // create smart collection
// $customcollection = [];
// $customcollection['smart_collection']['title'] = 'Customized Product 11';
// $customcollection['smart_collection']['rules'] = [];
// $customcollection['smart_collection']['rules'][0]['column'] = 'tag';
// $customcollection['smart_collection']['rules'][0]['relation'] = 'equals';
// $customcollection['smart_collection']['rules'][0]['condition'] = 'ProductCustomizer';

// $all_products = $shopify('POST /admin/api/'.API_VERSION.'/smart_collections.json', array(), $customcollection);    


// // order create webhook
// $count_order_hook = $shopify('GET /admin/api/2019-10/webhooks/count.json?topic=orders/create');
// if($count_order_hook == 0){
//   $shopify('POST /admin/api/2019-10/webhooks.json', array() ,  array('webhook' => 
//     array(
//     'topic'   => 'orders/create',
//     'address' =>  SHOPIFY_SITE_URL.'order_create_hook.php?shop='.$shop,
//     'format'  => 'json'
//     )));
// }
?>